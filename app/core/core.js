/**
 * Created by Aldenir on 2017-09-26.
 */

var core = angular.module('core', ['app']);

core.controller('coreController', ['$scope', '$cookieStore', 'appConfig', 'appModel', 'appManager', function ($scope, $cookieStore, appConfig, appModel, appManager) {
    appManager.init(appConfig, $cookieStore, $scope, appModel, false);

    $scope.hello = 'Hello world home'
}]);