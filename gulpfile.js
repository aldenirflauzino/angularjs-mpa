// gulp
var gulp = require('gulp');
var bs = require('browser-sync').create();

// plugins
var connect = require('gulp-connect');
gulp.task('serve', function() {
    bs.init({
        server: {
            baseDir: "./"
        },
         middleware: function(req,res,next) {
            if (req.url === '/home') {
              req.url = '/modules/home/home.html';
            } else if (req.url === '/') {
              req.url = '/modules/signin/signin.html';
            }else if (req.url === '/signin') {
              req.url = '/modules/signin/signin.html';
            } else if (req.url === '/jobs') {
              req.url = '/pages/jobs.html';
            }
            return next();
          }
    });
});
